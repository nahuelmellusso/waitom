<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Routes with not middleware
 */

# Companies
Route::get('companies', 'Api\CompaniesController@index');
Route::get('companies/{slug}', 'Api\CompaniesController@getCompanyData');




Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', 'Auth\UserController@current');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    /**
     * Chats
     */
    Route::get('messages', 'Api\ChatsController@index');
    Route::get('messages/company/{company_id}/user/{user_id}', 'Api\ChatsController@fetchMessages');
    Route::post('messages', 'Api\ChatsController@sendMessage');
    Route::delete('messages/{id}', 'Api\ChatsController@destroy');

    /**
     * Turns , custom
     */
    Route::post('turns/next', 'Api\TurnsController@nextTurn');


    Route::resource('companies', 'Api\CompaniesController')->except(['index', 'show']);
    Route::resources([
        'turns' => 'Api\TurnsController',
        'companies' => 'Api\CompaniesController',
        'employees' => 'Api\EmployeesController',
        'premises' => 'Api\PremisesController',
        'spots' => 'Api\SpotsController',
        'schedules' => 'Api\SchedulesController',]);

});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});

