<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Company::class, function (Faker $faker) {
    $fantasy_name = $faker->slug;
    $slug = Str::slug($fantasy_name);
    return [
        'name' => $faker->company,
        'fantasy_name' => $faker->company,
        'slug' => $slug,
        'description' => $faker->text(rand(50,255)),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'email' => $faker->companyEmail,
        'profile_image' => '',
        'rating' => rand(10, 50) / 10
    ];
});
