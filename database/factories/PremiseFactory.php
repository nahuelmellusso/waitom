<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Premise;
use Faker\Generator as Faker;

$factory->define(Premise::class, function (Faker $faker) {
    return [
        //'company_id' => null, // populated in seeder
        'name' => $faker->name,
        'description' => $faker->text(rand(50,255)),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'manager' => $faker->firstName . ' ' . $faker->lastName,
    ];
});
