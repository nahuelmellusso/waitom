<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Spot;
use Faker\Generator as Faker;

$factory->define(Spot::class, function (Faker $faker) {
    return [
        //'premise_id' => null, // populated in seeder
        'description' => $faker->text(50),
    ];
});
