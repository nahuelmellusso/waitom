<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnsTable extends Migration
{
    public function up()
    {
        Schema::create('turn_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('turns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('queue_id')->index();
            $table->unsignedBigInteger('turn_status_id')->index();
            $table->integer('time_to_attend');
            $table->integer('order')->nullable();
            $table->dateTime('taken_at')->nullable();
            $table->dateTime('processed_at')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('turn_status_id')->references('id')->on('turn_statuses');
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('turn_statuses');
        Schema::dropIfExists('turns');
        Schema::enableForeignKeyConstraints();
    }
}
