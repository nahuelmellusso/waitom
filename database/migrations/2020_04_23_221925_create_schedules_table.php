<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('premise_id')->nullable()->unsigned()->index();
            $table->unsignedBigInteger('employee_id')->nullable()->unsigned()->index();
            $table->integer('day');
            $table->time('from');
            $table->time('to');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('active')->default(true);

            $table->foreign('premise_id')->references('id')->on('premises')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
