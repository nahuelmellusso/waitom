<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployesTable extends Migration
{

    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('premise_id')->unsigned()->index();
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->boolean('is_manager')->default(true);
            $table->boolean('active')->default(true);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('premise_id')->references('id')->on('premises')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
