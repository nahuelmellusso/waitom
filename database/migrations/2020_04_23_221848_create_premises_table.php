<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremisesTable extends Migration
{

    public function up()
    {
        Schema::create('premises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->unsigned()->index();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('address');
            $table->integer('average_time');
            $table->string('phone');
            $table->decimal('lat',10,2);
            $table->decimal('lng',10,2);
            $table->string('manager')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('premises');
    }
}
