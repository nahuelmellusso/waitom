<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQueuesTable extends Migration
{
    public function up()
    {
        Schema::create('queues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('premise_id');
            $table->unsignedBigInteger('spot_id');
            $table->float('average_time');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('active')->default(true);

            $table->foreign('premise_id')->references('id')->on('premises')->onDelete('cascade');
            $table->foreign('spot_id')->references('id')->on('spots')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::dropIfExists('queues');
    }
}
