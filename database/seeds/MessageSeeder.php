<?php

use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $merchant = \App\Models\Company::whereHas('users', function ($query) {
                $query->where('email', 'merchant@waitom.com');
            })->first();
        $client = \App\Models\User::where('email', 'client@waitom.com')->first();

        $faker = \Faker\Factory::create();

        foreach (range(0,10) as $item) {
            \App\Models\Message::query()
                ->create([
                    'company_id' => $merchant->id,
                    'user_id' => $client->id,
                    'sent_by_company' => rand(0,1),
                    'message' => $faker->realText(100)
                ]);
        }
    }
}
