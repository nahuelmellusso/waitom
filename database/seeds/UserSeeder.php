<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        /**
         * Add some clients
         */
        factory(\App\Models\User::class, 10)
            ->create()
            ->each(function ($user) {
            $user->assignRole('Merchant');
        });

        /**
         * Add some customers
         */
        factory(\App\Models\User::class, 50)
            ->create()
            ->each(function ($user) {
            $user->assignRole('Client');
        });
    }
}
