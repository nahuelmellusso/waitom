<?php

use App\Models\TurnStatus;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Turn;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class TurnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear DB
         */
        Schema::disableForeignKeyConstraints();
        DB::table('turn_statuses')->truncate();
        DB::table('turns')->truncate();
        Schema::enableForeignKeyConstraints();


        $statuses = [
            TurnStatus::IN_QUEUE => 'In queue',
            TurnStatus::ATTENDING => 'Attending',
            TurnStatus::PROCESSED => 'Processed'
        ];
        foreach ($statuses as $status) {
            TurnStatus::query()->create([
                'name' => $status
            ]);
        }


        /**
         * Qty of turns to create
         */
        $qtyTurnsToCreate = 5;

        /**
         * Get random users. normal users only
         */
        $users = User::query()
            ->role('Client')
            ->inRandomOrder()
            ->take($qtyTurnsToCreate)
            ->get();


        foreach (range(1, $qtyTurnsToCreate) as $key => $order) {
            $user = $users[$key];
            Turn::query()->create([
                'user_id' => $user->id,
                'queue_id' => 1, // always assign turns to 1 queue for development ??  dunno
                'order' => $order,
                'turn_status_id' => TurnStatus::IN_QUEUE,
                'taken_at' => null,
                'processed_at' => null
            ]);
        }
    }
}
