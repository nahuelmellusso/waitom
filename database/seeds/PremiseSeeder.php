<?php

use Illuminate\Database\Seeder;
use App\Models\Queue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class PremiseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear DB
         */
        Schema::disableForeignKeyConstraints();
        DB::table('premises')->truncate();
        Schema::enableForeignKeyConstraints();

        /**
         * Add some companies
         */
        $companies = \App\Models\Company::query()->get();
        foreach ($companies as $company) {
            factory(\App\Models\Premise::class)->create([
                'company_id' => $company->id,
            ]);
        }


    }
}
