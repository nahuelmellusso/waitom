<?php

use Illuminate\Database\Seeder;
use App\Models\Queue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear DB
         */
        Schema::disableForeignKeyConstraints();
        DB::table('companies')->truncate();
        DB::table('company_user')->truncate();
        Schema::enableForeignKeyConstraints();

        /**
         * Add some companies
         */
        $qtyToCreate = 10;
        factory(\App\Models\Company::class, $qtyToCreate)
            ->create()
            ->each(function ($company) {
                $partnerUser = \App\Models\User::query()
                    ->role('Merchant')
                    ->inRandomOrder()
                    ->first();
                $company->users()->attach($partnerUser->id);

            });
    }
}
