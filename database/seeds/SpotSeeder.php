<?php

use Illuminate\Database\Seeder;
use App\Models\Queue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class SpotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear DB
         */
        Schema::disableForeignKeyConstraints();
        DB::table('spots')->truncate();
        Schema::enableForeignKeyConstraints();


        /**
         * Add some companies
         */
        $premises = \App\Models\Premise::all();
        foreach ($premises as $premise) {
            factory(\App\Models\Spot::class)->create([
                'premise_id' => $premise->id,
            ]);
        }


    }
}
