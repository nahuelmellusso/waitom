<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(PremiseSeeder::class);
        $this->call(SpotSeeder::class);
        $this->call(QueueSeeder::class);
        $this->call(TurnSeeder::class);


        // User that has a company.
        $queue = \App\Models\Queue::query()
            ->whereHas('turns')
            ->with('premise.company.users')
            ->first();

        \App\Models\User::query()
            ->where('id', $queue->premise->company->users[0]->id)
            ->first()
            ->update([
                'email' => 'merchant@waitom.com',
            ]);


        // user that has a turn.
        \App\Models\User::query()
            ->whereHas('turns')
            ->first()
            ->update([
                'email' => 'client@waitom.com',
            ]);



    }
}
