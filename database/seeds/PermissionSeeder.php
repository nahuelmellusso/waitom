<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear DB
         */
        Schema::disableForeignKeyConstraints();

        DB::table('model_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('roles')->truncate();
        DB::table('role_has_permissions')->truncate();
        DB::table('permissions')->truncate();

        Schema::enableForeignKeyConstraints();

        $roles_permissions = [
            'Client' => [],
            'QueueManager' => [],
            'Merchant' => [
                // models => actions
                'user' => ['create', 'edit', 'delete', 'see'],
            ],
        ];

        $models_permissions = array_keys($roles_permissions['Merchant']);

        foreach ($models_permissions as $models_permission) {
            foreach (['create', 'edit', 'delete', 'see', 'import'] as $action) {
                Permission::create(['name' => $action . '_' . $models_permission]);
            }
        }


        foreach ($roles_permissions as $r => $role_permission) {
            $role = Role::create(['name' => $r]);

            foreach ($role_permission as $model => $permissions) {
                foreach ($permissions as $permission) {
                    $role->givePermissionTo($permission . '_' . $model);
                }
            }

        }
    }
}
