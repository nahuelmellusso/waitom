<?php

use Illuminate\Database\Seeder;
use App\Models\Queue;
use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class QueueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear DB
         */
        Schema::disableForeignKeyConstraints();
        DB::table('queues')->truncate();
        Schema::enableForeignKeyConstraints();

        $spot = \App\Models\Spot::query()
            ->inRandomOrder()
            ->first();

        // creating just 1 queue for now?
        Queue::query()->create([
            'premise_id' => $spot->premise_id,
            'spot_id' => $spot->id,
            'average_time' => rand(15, 20), // minutes?
        ]);
    }
}
