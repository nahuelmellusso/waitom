// import axios from 'axios'
// import Cookies from 'js-cookie'

const notifications = {

  data () {
    return {}
  },
  created: function () {
  },
  methods: {
    listenChatList: function (companyId, callback) {
      window.Echo.private(`ChatListDashboard.${companyId}`)
        .listen('.App\\Events\\ChatListDashboard', (event) => {
          this.$store.commit('chat/pushChat', event.newChat)
          console.log('.App\\Events\\ChatListDashboard', event)
          if (callback) {
            callback(event)
          }
        })
    },
    listenMessageSent: function (userId, companyId, callback) {
      window.Echo.private(`Chat.${userId}.${companyId}`)
        .listen('.App\\Events\\MessageSent', (event) => {
          this.$store.commit('chat/pushMessage', event.message)
          console.log('Event Triggered', event)
          if (callback) {
            callback(event)
          }
        })
    },
    listenNotification: function (resourceType, resourceId, callback) {
      window.Echo.private(`Notification.${resourceType}.${resourceId}`)
        .listen('.App\\Events\\Notification', (event) => {
          console.log('Event Triggered', event)
          callback(event)
        })
    }
  }

}

export default notifications
