import store from '~/store'

export default (to, from, next) => {
  let currentUser = store.getters['auth/user']
  let can = false;
  currentUser.roles.forEach((role) => {
    console.log('LOG', role.name);
    if (role.name === 'Merchant') {
      can = true;
    }
  })
  if (can) {
    next()
  } else {
    next({ name: 'home' })
  }
}
