import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import Permissions from './mixins/permissions'
import Notifications from './mixins/notifications'
import Helpers from './mixins/helpers'

import '~/plugins'
import '~/components'

Vue.config.productionTip = false

Vue.mixin(Permissions)
Vue.mixin(Notifications)
Vue.mixin(Helpers)

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
