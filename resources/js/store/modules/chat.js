import axios from 'axios'
import collect from 'collect.js'

export const state = {
  messages: [],
  chats: [],
  userCompany: null
}

export const getters = {
  messages: state => state.messages,
  chats: state => state.chats,
  userCompany: state => state.userCompany
}

export const mutations = {
  pushMessage (state, message) {
    state.messages.push(message)
  },
  pushChat (state, chat) {
    let chatExists = collect(state.chats)
      .where('user_id', chat.user_id)
      .where('company_id', chat.company_id)
      .first()
    console.log(chatExists)
    if (chatExists) {
      chatExists.unread = true
    } else {
      state.chats.unshift(chat)
    }
  },
  /**
   * Used to get current user - company on messages.
   * @param state
   * @param userCompany
   */
  setUserCompany (state, userCompany) {
    state.userCompany = userCompany
  },
  setAsRead (state, userCompany) {
    let key = collect(state.chats)
      .where('user_id', userCompany.user_id)
      .where('company_id', userCompany.company_id)
      .first()
    if (key) {
      key.unread = false
    }
  }
}

export const actions = {
  fetchChats ({ state }) {
    axios.get('/api/messages/').then(response => {
      state.chats = response.data
    })
  },
  fetchMessages ({ commit, state }, info) {
    commit('setUserCompany', info)
    commit('setAsRead', info)
    axios.get(`/api/messages/company/${info.company_id}/user/${info.user_id}`).then(response => {
      state.messages = response.data.data
    })
  },
  addMessage ({ commit, state }, message) {
    axios.post('/api/messages', message)
      .then(response => {
        // Its triggered from Messages.vue common controller.
        // By listening socket channel
        // commit('pushMessage', message)
      })
  }
}
