import genericModule from './../resourceController'

const ResourceController = genericModule('turns', { role: '' })

export const state = {
  ...ResourceController.state
}
export const getters = {
  ...ResourceController.getters
}
export const mutations = {
  ...ResourceController.mutations
}
export const actions = {
  ...ResourceController.actions
}
