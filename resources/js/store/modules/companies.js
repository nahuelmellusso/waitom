import genericModule from './../resourceController'
import axios from 'axios'

const ResourceController = genericModule('companies', { role: '' })

export const state = {
  ...ResourceController.state,
  company: null
}
export const getters = {
  ...ResourceController.getters,
  company: state => state.company
}
export const mutations = {
  ...ResourceController.mutations,
  setCompany (state, company) {
    state.company = company
  }
}
export const actions = {
  ...ResourceController.actions,
  /**
   * Get Company data by slug
   * Executed when page loads
   * @param commit
   * @param state
   * @param slug
   * @returns {Promise.<void>}
   */
  async getCompanyData ({ commit, state }, slug) {
    try {
      const { data } = await axios.get(`/api/${state.resource}/${slug}`)
      commit('setCompany', data.data)
    } catch (e) {
      state.company = null
    }
  }
}
