function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  /**
   * Auth
   */
  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  /**
   * Front end
   */
  { path: '/home', redirect: { name: 'home' } },
  { path: '/', name: 'home', component: page('home.vue') },

  /**
   * Clients sections
   */
  { path: '/merchant/:slug',
    component: page('merchant/index.vue'),
    children: [
      { path: '', redirect: { name: 'merchant.shop' } },
      { path: 'home', name: 'merchant.home', component: page('merchant/home.vue') },
      { path: 'about', name: 'merchant.about', component: page('merchant/about.vue') },
      { path: 'messages', name: 'merchant.messages', component: page('merchant/messages.vue') },
      { path: 'what-s-new', name: 'merchant.what-s-new', component: page('merchant/what-s-new.vue') }
    ] },
  /**
   * Loggued users
   */
  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] },
  /**
   * Dashboard Area
   */
  { path: '/dashboard',
    name: 'dashboard.index',
    component: page('dashboard/index.vue'),
    children: [
      { path: 'business', name: 'dashboard.business', component: page('dashboard/business.vue') },
      { path: 'messages', name: 'dashboard.messages', component: page('dashboard/messages.vue') },
      { path: 'staff', name: 'dashboard.staff', component: page('dashboard/business.vue') },
      { path: 'waiting-rooms', name: 'dashboard.waiting-rooms', component: page('dashboard/business.vue') }
    ] },

  /**
   * Default 404
   */
  { path: '*', component: page('errors/404.vue') }
]
