import Echo from 'laravel-echo'
import Cookies from 'js-cookie'

window.Pusher = require('pusher-js')

let token = Cookies.get('token')

window.Echo = new Echo({
  broadcaster: 'pusher',
  key: '07cf0a6f9c597417169a',
  cluster: 'ap1',
  authEndpoint: '/broadcasting/auth',
  // As I'm using JWT tokens, I need to manually set up the headers.
  auth: {
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json'
    }
  }
})

