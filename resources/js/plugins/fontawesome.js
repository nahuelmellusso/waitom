import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import * as Regular from '@fortawesome/free-regular-svg-icons'

import * as Solid from '@fortawesome/free-solid-svg-icons'

import {
  faGithub
} from '@fortawesome/free-brands-svg-icons'

Vue.component('fa', FontAwesomeIcon)

library.add(
  faGithub,

  Solid.faUser,
  Solid.faLock,
  Solid.faSignOutAlt,
  Solid.faCog,
  Solid.faHammer,
  Solid.faChartLine,
  Solid.faBuilding,
  Solid.faUserNinja,
  Solid.faHeadset,
  Solid.faUserClock,

  Regular.faEnvelope,
  Regular.faComments
)
