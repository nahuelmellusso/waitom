<?php


namespace App\Services;

use App\Models\Premise;
use App\Models\Queue;
use App\Models\Turn;
use function foo\func;

class FifoTurnService
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function getTurn(){
        // get the current premise
        $premise = Premise::find($this->request->premise_id);

        // validate if there any turn with the current user request and if a valid schedule
        if(!$this->validateTurn($this->request->user_id, $premise)){
            return response()->json(['msg' => trans('turns.user_has_turn')],422);
        }
        // find queue to assign , searching wich queue has less turns
        $queue = $this->getQueueToAssign($premise);
//        $this->getTimeToAttend($queue);
        // create new turn , calculating time to attend
        $turn = Turn::create([
                'user_id' => $this->request->user_id,
                'queue_id' => $queue->id,
                'turn_status_id' => 1,
                'time_to_attend' => $this->getTimeToAttend($queue),
                'order' => $queue->next_order,
                'taken_at' => now(),
            ]);
        // update queue average time
        $this->updateAverageTime($queue);
        return response()->json($turn);
    }

    // returns a queue to assign turn
    private function getQueueToAssign($premise){
        // if there aren t queues then initialize them, (initialize should  be here ?)
        if(!$premise->active_queues->count()){
            return $this->initializeSpots($premise);
        }
        //  return queue with less turns
        return $premise->next_queue;
    }

    private function initializeSpots($premise){
        $premise->queues()->delete();
        $premise->spots->map(function($spot) use ($premise){
            $queue = Queue::create([
                'premise_id' => $premise->id,
                'spot_id' => $spot->id,
                'average_time' => $premise->average_time,
            ]);
        });

        return $premise->random_queue;
    }

    private function validateTurn($user_id,$premise){
        $userTurns =  $premise->queues->filter(function($queue) use ($user_id){
            return $queue->turns()
                ->where('user_id',$user_id)
                ->whereDate('created_at', now()->format('Y-m-d'))
                ->first();
        });

        if($userTurns->count()) return false;

        return $premise->has_available_schedule;
    }

    private function getTimeToAttend($queue){
        if(!$queue->turns->count()) return 1;
        return $queue->turns->count() * $queue->average_time;
    }

    private function updateTimeToAttend($queue){
        $turns = $queue->turns()->orderBy('order','asc')->get();
        $time = $turns->first()->remaning_time;
        $turns->first->shift();
        $turns->map(function($turn) use ($queue){
            $turn->time_to_attend = $turn->order * $queue->average_time;
            $turn->save();
        });
    }

    public function getNextTurn(){
        // find the attended turn
        $current_turn = Turn::find($this->request->current_turn_id);
        // get the current queue tu assign
        $queue = $current_turn->queue;
        // set turn ending time
        $current_turn->processed_at = now();
        //remove from queue
        $current_turn->delete();

        // get the next turn in queue
        $next_turn = $queue->next_turn;
        // set the attended time
        $next_turn->taken_at = now();
        $next_turn->save();
        // update queue average time
        $this->updateAverageTime($queue);

        return $next_turn;
    }

    private function updateAverageTime($queue){
        // get attended turns
        $turns = $queue->attended_turns;
        // calculate new average time
        $average = $turns->sum('time_elapsed') / $turns->count();
        $queue->average_time = $average;
        $queue->save();
    }

    public function deleteTurn(){
        // get turn to delete
        $turn = Turn::find($this->request->turn_id);
        // get turn queue
        $queue = $turn->queue;
        // delete turn from queue
        $turn->delete();
        // update average time
        $this->updateAverageTime($queue);
        return true;
    }

    public function reorderTurns(){

    }
}
