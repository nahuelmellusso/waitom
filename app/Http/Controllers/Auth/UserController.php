<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get authenticated user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function current(Request $request)
    {
        $user = $request->user();
        $user->load('roles.permissions');
        if ($user->hasRole('Merchant')) {
            $user->load('companies');
        }
        return response()->json($user);
    }

    //    public function loginAsUser(Request $request)
//    {
//        if (App::environment(['production', 'prod'])) {
//            abort(401);
//        }
//
//        $this->validate($request, [
//            'user_id' => 'required|integer|exists:users,id'
//        ]);
//
//        if (Auth::guard('api')->user()->hasRole('administrator')) {
//            $admin_id = Auth::guard('api')->user()->id;
//            setcookie('switchBackAdmin', $admin_id, -1, '/');
//        }
//
//        Auth::guard('api')->logout(); // for end current session
//        $auth_token = Auth::guard('api')->tokenById($request->user_id);
//        Auth::guard('api')->setToken($auth_token);
//
//        $expiration = Auth::guard('api')->getPayload()->get('exp');
//
//        if (Auth::guard('api')->user()->hasRole('administrator')) {
//            setcookie('switchBack', false, -1, '/');
//            unset($_COOKIE['switchBack']);
//        } else {
//            $admin_id = $_COOKIE['switchBackAdmin'];
//            setcookie('switchBack', $admin_id, $expiration ?? 365, '/');
//            unset($_COOKIE['switchBackAdmin']);
//        }
//
//
//        return response()->json([
//            'token' => $auth_token,
//            'token_type' => 'bearer',
//            'expires_in' => $expiration - time(),
//        ]);
//    }

}
