<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ResourceController;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;

class EmployeesController extends ResourceController
{

    protected $resourceModel = 'App\Models\Employee';
    protected $paginate = false;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function afterIndex($query)
    {
        return $query;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'premise_id' => 'required|integer|exists:premises,id',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:employees',
        ]);

        $employee = Employee::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'premise_id' => $request->premise_id
        ]);
        //$role = $request->is_manager ? 'Manager' : 'Employee';
        //$employee->assignRole($role);

        return response()->json($employee,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $this->validate($request, [
            'premise_id' => 'required|integer|exists:premises,id',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:employees',
        ]);

        $employee->update($request->all());

        return response()->json($employee, 200);
    }

}
