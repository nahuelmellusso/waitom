<?php

namespace App\Http\Controllers\Api;

use App\Console\Commands\refresh;
use App\Events\UserNotification;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Turn;
use App\Services\FifoTurnService;
use Illuminate\Http\Request;

/**
 * Class TurnsController
 * @package App\Http\Controllers\Admin
 */
class TurnsController extends ResourceController
{
    protected $resourceModel = 'App\Models\Turn';
    protected $paginate = true;

    /**
     * Add extra logic to GET requests
     *
     * @param $query
     * @return mixed
     */
    protected function afterIndex($query)
    {
        // Example
        // loads all the turns that belongs to current user
        if ($this->authenticated->hasRole('Client')) {
            $query = $query->where('user_id', $this->authenticated->id);
        }

        // or
        //$query = $query->whereHas('spot', function($q) {
        //    $q->where('active', 1); // or whatever
        //});

        // or
        // Send a notification or whatever...
        //event(new UserNotification('Eze!'));

        return $query;
    }

    public function store(Request $request)
    {
        $service = new FifoTurnService($request);
        return $service->getTurn();
    }

    public function nextTurn(Request $request){
        $service = new FifoTurnService($request);
        return $service->getNextTurn();
    }

    public function cancelTurn(Request $request){
        $service = new FifoTurnService($request);
        return $service->deleteTurn();
    }

    public function reorderTurns(Request $request){
        $service = new FifoTurnService($request);
        return $service->reorderTurns();
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:turns,id',
            'user_id' => 'required|integer|exists:users,id',
            //....
        ]);

        $turn = Turn::where('id', $id)->first();
        $turn->update($request->all());

        return response()->json($turn, 200);
    }


}
