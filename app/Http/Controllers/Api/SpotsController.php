<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\ResourceController;
use Illuminate\Http\Request;
use App\Models\Spot;

class SpotsController extends ResourceController
{

    protected $resourceModel = 'App\Models\Spot';
    protected $paginate = false;

    protected function afterIndex($query)
    {
        return $query;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'premise_id' => 'required|integer|exists:premises,id',
            'description' => 'required|string|max:255',
        ]);

        $spot = Spot::create([
            'premise_id' => $request->premise_id,
            'description' => $request->description
        ]);

        return response()->json($spot);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Spot $spot)
    {
        $this->validate($request, [
            'premise_id' => 'required|integer|exists:premises,id',
            'description' => 'required|string|max:255',
        ]);

        $spot->update([
            'premise_id' => $request->premise_id,
            'description' => $request->description,
            'active' => $request->active

        ]);

        return response()->json($spot);
    }


}
