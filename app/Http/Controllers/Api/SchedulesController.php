<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ResourceController;
use App\Models\Schedule;
use Illuminate\Http\Request;



class SchedulesController extends ResourceController
{

    protected $resourceModel = 'App\Models\Schedule';
    protected $paginate = false;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function afterIndex($query)
    {
        return $query;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'premise_id' => 'required_without:employee_id|exists:premises,id',
            'employee_id' => 'required_without:premise_id|exists:employees,id',
//            'day' => 'required|integer',
            'schedules.*' => 'required',
//            'schedules.to' => 'required',
//            'schedules.day' => 'required'
        ]);


        foreach($request->schedules as $schedule){
            Schedule::create([
                'premise_id' => $request->premise_id,
                'employee_id' => $request->employee_id,
                'day' => $schedule['day'],
                'from' => $schedule['from'],
                'to' => $schedule['to']
            ]);
        }

        return response()->json(true,200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {

        $this->validate($request, [
            'premise_id' => 'required_without:employee_id|exists:premises,id',
            'employee_id' => 'required_without:premise_id|exists:employees,id',
//            'day' => 'required|integer',
            'schedules.*' => 'required',
//            'schedules.to' => 'required',
//            'schedules.day' => 'required'
        ]);

        if($id = $request->premise_id){
            Schedule::where('premise_id',$id)->delete();
        }

        if($id = $request->employee_id){
            Schedule::where('employee_id',$id)->delete();
        }


        foreach($request->schedules as $schedule){
            Schedule::create([
                'premise_id' => $request->premise_id,
                'employee_id' => $request->employee_id,
                'day' => $schedule['day'],
                'from' => $schedule['from'],
                'to' => $schedule['to']
            ]);
        }

        return response()->json(true,200);
    }
}
