<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\ResourceController;
use App\Models\Premise;
use Illuminate\Http\Request;

class PremisesController extends ResourceController
{

    protected $resourceModel = 'App\Models\Premise';
    protected $paginate = false;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function afterIndex($query)
    {
        return $query;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required|integer|exists:companies,id',
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'lat' => 'required|max:12',
            'lng' => 'required|max:12',
            'manager' => '',
        ]);

        $premise = Premise::create([
            'company_id' => $request->company_id ,
            'name' => $request->name ,
            'description' => $request->description ,
            'address' => $request->address ,
            'phone' => $request->phone ,
            'lat' => $request->lat ,
            'lng' => $request->lng ,
            'manager' => $request->manager ,
        ]);


        return response()->json($premise,200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Premise $premise)
    {
        $this->validate($request, [
            'company_id' => 'required|integer|exists:companies,id',
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'lat' => 'required|max:12',
            'lng' => 'required|max:12',
            'manager' => '',
        ]);

        $premise->update($request->all());

        return response()->json($premise, 200);
    }

}
