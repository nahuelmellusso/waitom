<?php

namespace App\Http\Controllers\Api;

use App\Events\ChatListDashboard;
use App\Events\MessageSent;
use App\Events\Notification;
use App\Events\UserNotification;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Message;
use App\Models\Turn;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class ChatsController
 * @package App\Http\Controllers\Api
 */
class ChatsController extends Controller
{
    /**
     * @var mixed
     */
    private $authenticated;

    /**
     * ChatsController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->authenticated = $request->user();
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $messages = [];
        if ($this->authenticated->hasRole('Merchant')) {
            $messages = $this->_merchantChatList();
        } else if ($this->authenticated->hasRole('Client')) {
            $messages = $this->_clientChatList();
        }
        $result = $messages;
        return response()->json($result, 200);
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages(Request $request, $company_id, $user_id)
    {
        $company_id = filter_var($company_id, FILTER_SANITIZE_NUMBER_INT);
        $user_id = filter_var($user_id, FILTER_SANITIZE_NUMBER_INT);

        if ($this->authenticated->hasRole('Client')
            && $user_id != $this->authenticated->id) {
            abort(403);
        }
        if ($this->authenticated->hasRole('Merchant')
            && $this->authenticated->companies->where('id', $company_id) == null) {
            abort(403);
        }

        # Mark as read
        if ($this->authenticated->hasRole('Client')) {
            Message::query()
                ->where('user_id', $user_id)
                ->where('company_id', $company_id)
                ->where('sent_by_company', true)
                ->update([
                    'read_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
        }
        # mark as read
        if ($this->authenticated->hasRole('Merchant')) {
            Message::query()
                ->where('user_id', $user_id)
                ->where('company_id', $company_id)
                ->where('sent_by_company', false)
                ->update([
                    'read_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
        }

        $messages = Message::query()
            ->where('user_id', $user_id)
            ->where('company_id', $company_id)
            ->orderBy('created_at')
            ->limit(500)
            ->get();
        $result = [ 'data' => $messages ];
        return response()->json($result, 200);
    }

    /**
     * Persist message to database
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessage(Request $request)
    {
        $this->validate($request, [
            'message' => 'required|string',
            'user_id' => 'required|exists:users,id',
            'company_id' => 'required|exists:companies,id',
            'sent_by_company' => 'required|boolean',
        ]);

        if ($this->authenticated->hasRole('Client')
            && (
                $request->input('user_id') !== $this->authenticated->id
                || $request->input('sent_by_company')
            )) {
            abort(500, 'You are logged as Client. Unable to create');
        }

        if ($this->authenticated->hasRole('Merchant')
            && (
                !$request->input('sent_by_company')
                || !$this->authenticated->companies->contains('id', $request->input('company_id'))
            )) {
            abort(500, 'You are logged as Merchant. Unable to create');
        }

        /**
         * Broadcasting Socket messaging both endpoints
         * in case they have the chats opened
         */
        $message = Message::create([
                'message' => $request->input('message'),
                'company_id' => $request->input('company_id'),
                'user_id' => $request->input('user_id'),
                'sent_by_company' => $request->input('sent_by_company'),
            ]);
        broadcast(new MessageSent($this->authenticated, $message));

        /**
         * Broadcast Update Client list
         */
        $newChat = [
            'user_id' => $message->user_id,
            'company_id' => $message->company_id,
            'company' => Company::find($message->company_id)->toArray(),
            'user' => User::find($message->user_id)->toArray(),
            'unread' => true
        ];
        broadcast(new ChatListDashboard($newChat));

        /**
         * Send notification, new message
         */
        $notification = [
            'resource_type' => '',
            'resource_id' => '',
            'notification_type' => 'chat',
            'notification_content' => $message
        ];
        if ($this->authenticated->hasRole('Merchant')) {
            $notification['resource_type'] = 'Client';
            $notification['resource_id'] = $message->user_id;
            event(new Notification($notification));
        }
        if ($this->authenticated->hasRole('Client')) {
            $notification['resource_type'] = 'Merchant';
            $notification['resource_id'] = $message->company_id;
            event(new Notification($notification));
        }


        $result = ['status' => 'Message Sent!'];
        return response()->json($result, 200);
    }

    private function _chatAlreadyExists($message)
    {
        $count = Message::query()
            ->where('user_id', $message->user_id)
            ->where('company_id', $message->company_id)
            ->count();
        if ($count > 1) {
            return true;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function _merchantChatList()
    {
        $companies_ids = $this->authenticated
            ->companies
            ->keyBy('id')
            ->keys()
            ->toArray();
        $messages = Message::query()
            ->select('user_id', 'company_id')
            ->distinct()
            ->whereIn('company_id', $companies_ids)
            ->limit(500)
            ->with([
                'company',
                'user' // would be users that merchant has chat with
            ])
            ->get()
            ->toArray();

        foreach ($messages as &$message) {
            $undead = Message::query()
                ->where('user_id', $message['user_id'])
                ->where('company_id', $message['company_id'])
                ->where('sent_by_company', false)
                ->whereNull('read_at')
                ->count();
            $message['unread'] = $undead > 0;
        }
        return $messages;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function _clientChatList()
    {
        $messages = Message::query()
            ->select('company_id')
            ->distinct()
            ->where('user_id', $this->authenticated->id)
            ->with('company')
            ->limit(100)
            ->get();
        return $messages;
    }
}
