<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Company;
use App\Models\Turn;
use Illuminate\Http\Request;

/**
 * Class TurnsController
 * @package App\Http\Controllers\Admin
 */
class CompaniesController extends ResourceController
{
    protected $resourceModel = 'App\Models\Company';
    protected $paginate = false;

    /**
     * Add extra logic to GET requests
     *
     * @param $query
     * @return mixed
     */
    protected function afterIndex($query)
    {
        return $query;
    }


    public function getCompanyData(Request $request, $slug)
    {
        $slug = filter_var($slug, FILTER_SANITIZE_STRING);
        $company = Company::query()
            ->with('premises')
            ->where('slug', $slug)
            ->first();

        $result = [ 'data' => $company ];
        return response()->json($result, 200);
    }


    /**
     * Store
     * @param Request $request
     */
    public function store(Request $request)
    {
//        $this->validate($request, [
//            'user_id' => 'required|integer|exists:users,id',
//            //....
//        ]);
//
//        $turn = Company::query()->create($request->all());
//        return response()->json($turn, 200);
    }

    /**
     * Update
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
//        $this->validate($request, [
//            'id' => 'required|integer|exists:turns,id',
//            'user_id' => 'required|integer|exists:users,id',
//            //....
//        ]);
//
//        $turn = Company::query()->where('id', $id)->first();
//        $turn->update($request->all());
//
//        return response()->json($turn, 200);
    }


}
