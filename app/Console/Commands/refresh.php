<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class refresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh {--dump-autoload}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Development refresh database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dump_autoload = $this->option('dump-autoload');
        if ($dump_autoload) {
            exec('composer dump-autoload');
        }

        # fresh migrations
        $this->call('migrate:fresh');
        $this->call('db:seed');
        $this->call('config:cache');
    }
}
