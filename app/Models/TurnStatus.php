<?php

namespace App\Models;

/**
 * Class TurnStatus
 * @package App\Models
 */
class TurnStatus extends ModelScope
{
    const IN_QUEUE = 1;
    const ATTENDING = 2;
    const PROCESSED = 3;

    protected $fillable = [
        'name',
    ];


    public function turns()
    {
        $this->hasMany(Turn::class);
    }
}
