<?php

namespace App\Models;

use GuzzleHttp\Promise\Promise;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends ModelScope
{
    use SoftDeletes;

    protected $fillable = [
        'premise_id',
        'name',
        'phone',
        'email',
    ];

    public static $loadable = [
        'premises',
    ];


    public function premises()
    {
        return $this->belongsToMany(Premise::class);
    }
}
