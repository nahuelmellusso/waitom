<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Company
 * @package App\Models
 */
class Company extends ModelScope
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'name',
        'fantasy_name',
        'slug',
        'description',
        'address',
        'phone',
        'email',
        'profile_image',
        'rating',
        'active',
    ];

    public static $loadable = [
        'users',
        'premises',
    ];

    /**
     * Array with fields/relations that can be used for sorting.
     *
     * @var array
     */
    public static $sortable = [
        'name',
        'fantasy_name',
        'address',
        'phone',
        'email', //for nested relationships, see commented scope function below for example
        'rating' //for nested relationships, see commented scope function below for example
    ];


    public function users()
    {
        return $this->belongsToMany(User::class);
    }


    public function premises()
    {
        return $this->hasMany(Premise::class);
    }


    public function spots()
    {
        return $this->hasManyThrough(Spot::class, Premise::class);
    }

}
