<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Queue extends ModelScope
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'premise_id',
        'spot_id',
        'average_time',
        'active',
    ];

    public function getNextOrderAttribute(){
        return $this->turns()->max('order') + 1;
    }

    public function getCountTurnsAttribute(){
        return $this->turns->count();
    }

    public function getNextTurnAttriubte(){
        return $this->turns()
            ->orderBy('order','ASC')
            ->first();
    }

    public function getAtendedTurnsAttribute(){
        return $this->turns()
            ->whereDate('created_at',now()->format('Y-m-d'))
            ->onlyTrashed()
            ->get();
    }
    /**
     * Premise Relation
     */
    public function premise()
    {
        return $this->belongsTo(Premise::class);
    }

    /**
     * Turns Relation
     */
    public function turns()
    {
        return $this->hasMany(Turn::class);
    }

}
