<?php

namespace App\Models;

use App\Traits\Filterable;
use App\Traits\Sortable;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Turn
 * @package App\Models
 */
class Turn extends ModelScope
{
    use SoftDeletes;
    use Filterable;
    use Sortable;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'queue_id',
        'order',
        'turn_status_id',
        'time_to_attend',
        'taken_at',
        'processed_at',
        'active',
    ];

    /**
     * Array with all the relations that can be loaded by API.
     *
     * @var array
     */
    public static $loadable = [
        'queue',
        'user',
        'queue.premise' // for nested relationships
    ];


    /**
     * Array with fields/relations that can be used for sorting.
     *
     * @var array
     */
    public static $sortable = [
        'user_id',
        'queue_id',
        'taken_at',
        'processed_at',
        'user.first_name' //for nested relationships, see commented scope function below for example
    ];


    /**
     * Sort field in relationship
     * Example: for Users relationship: First name field.
     *
     * @param $query
     * @param $field
     * @param $type
     * @return mixed
     */
//    public function scopeOrderByUser($query, $field, $type)
//    {
//        return $query->leftJoin('users', 'turns.user_id', '=', 'users.id')
//            ->orderBy('users.' . $field, $type);
//    }

    protected $dates = [
        'taken_at',
        'processed_at',
    ];

    public function getTimeElapsedAttribute(){
        return $this->processed_at->diffInMinutes($this->taken_at);
    }

    public function getRemaningTime(){
        $remaning = $this->queue->average_time->diffInMinutes($this->taken_at);
        return $remaning >= 0 ? $remaning : 0 ;
    }
    /**
     * User Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Queue Relation
     */
    public function queue()
    {
        return $this->belongsTo(Queue::class);
    }


    /**
     * Turn Statuses Relation
     */
    public function turnStatus()
    {
        return $this->belongsTo(TurnStatus::class);
    }

}
