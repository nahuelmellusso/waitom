<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends ModelScope
{
    use SoftDeletes;

    protected $fillable = [
        'premise_id',
        'employee_id',
        'day',
        'from',
        'to',
    ];

    public static $loadable = [
        'premises',
        'employees'
    ];

    protected $dates = [
        'from',
        'to',
    ];

    protected $casts = [
        'from'  => 'date:H:i',
        'to' => 'datetime:H:i',
    ];

    public function employees()
    {
        return $this->hasOne(Employee::class);
    }

    public function premises()
    {
        return $this->belongsToMany(Premise::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }


}
