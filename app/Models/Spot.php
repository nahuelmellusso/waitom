<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Spot
 * @package App\Models
 */
class Spot extends ModelScope
{
    use SoftDeletes;

    protected $fillable = [
        'premise_id',
        'description',
        'active'
    ];

    public function premise()
    {
        return $this->belongsTo(Premise::class);
    }


    public function queues()
    {
        return $this->hasMany(Queue::class);
    }

}
