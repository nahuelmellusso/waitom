<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Company
 * @package App\Models
 */
class Message extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['read_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'company_id',
        'user_id',
        'message',
        'read_at',
        'sent_by_company'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
