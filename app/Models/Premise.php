<?php

namespace App\Models;

use App\Http\Controllers\Admin\SpotsController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Premise extends ModelScope
{
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'name',
        'description',
        'address',
        'phone',
        'lat',
        'lng',
        'manager',
        'active',
    ];



    public function getNextQueueToAssignAttribute(){
//       dd($this->queues()->turns);
        return $this->queues()
            ->whereDate('queues.created_at', now()->format('Y-m-d'))
            ->orderBy('turns_count','desc')
            ->withCount('turns')
            ->get();
    }

    //get active queues
    public function getActiveQueuesAttribute(){
        return $this->queues()
            ->whereDate('queues.created_at', now()->format('Y-m-d'))
            ->withCount('turns')
            ->get();
    }

    // get day schedule
    public function getCurrentSchedulesAttribute(){
        return $this->schedules()
            ->where('day',now()->format('d'))
            ->get();
    }

    public function getRandomSpotAttribute(){
        return $this->spots()->orderByRaw('RAND()')->take(1)->first();
    }

    public function getNextQueueAttribute(){
        return $this->queues()
            ->withCount('turns')
            ->orderBy('turns_count','asc')
            ->first();
    }

    public function getRandomQueueAttribute(){
        return $this->queues()->orderByRaw('RAND()')->take(1)->first();
    }

    public function getHasAvailableScheduleAttribute(){
        return $this->schedules()
            ->where('day',now()->format('d'))
            ->get()
            ->filter(function ($schedule) {
                return $schedule->from->format('H:i') <= now()->format('H:i')
                    && $schedule->to->format('H:i') >= now()->format('H:i');
                })->count() == 0 ? false: true;
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }


    public function spots()
    {
        return $this->hasMany(Spot::class);
    }


    public function queues()
    {
        return $this->hasManyThrough(Queue::class, Spot::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }


}
