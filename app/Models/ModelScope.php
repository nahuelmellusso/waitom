<?php
namespace App\Models;

use App\Notifications\VerifyEmail;
use App\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class ModelScope
 * @package App\Models
 */
class ModelScope extends Model //, MustVerifyEmail
{

    /**
     * Add global scope
     *
     * ??
     */

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            return $builder->orderByDesc('active');
        });

//        static::addGlobalScope($table.'active', function ($query) {
//            return $query->where(self::getTableName().'.active', true);
//        });
    }

    /**
     * With and where has
     *
     * Usage: ->withAndWhereHas('Relation', function ($query) {});
     *
     * @param $query
     * @param $relation
     * @param $constraint
     * @return mixed
     */
    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)
            ->with([$relation => $constraint]);
    }
}
