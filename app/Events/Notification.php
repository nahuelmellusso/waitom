<?php

namespace App\Events;

use App\Models\Message;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Notification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Message details
     *
     * @var Message
     */
    public $notification;

    /**
     * Notification constructor.
     * @param $notification
     */
    public function __construct($notification)
    {
        $this->notification = $notification;

        /**
         * Validate that the information is going through
         */
        if (
            empty($this->notification['resource_type'])
            || empty($this->notification['resource_id'])
            || empty($this->notification['notification_type'])
            || empty($this->notification['notification_content'])
            || !in_array($this->notification['resource_type'], ['Client', 'Merchant'])
        ) {
            abort(500);
        }

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("Notification.{$this->notification['resource_type']}.{$this->notification['resource_id']}" );
    }

}
